import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./ter/components/login/login.component";
import {MainComponent} from "./ter/components/main/main.component";
import {authGuard} from "./ter/guards/auth.guard";
import {UserComponent} from "./ter/components/user/user.component";
import {TerComponent} from "./ter/components/ter/ter.component";
import {TerListComponent} from "./ter/components/ter-list/ter-list.component";
import {NewUserComponent} from "./ter/components/new-user/new-user.component";
import {SelfRatingComponent} from "./ter/components/self-rating/self-rating.component";

const routes: Routes = [
  {path: '', component: LoginComponent}, //default route
  {path: 'main', component: MainComponent, canActivate: [authGuard]},
  {path: 'user', component: UserComponent, canActivate: [authGuard]},
  {path: 'ter', component: TerComponent, canActivate: [authGuard]},
  {path: 'teacherlist', component: TerListComponent, canActivate: [authGuard]},
  {path: 'newuser', component: NewUserComponent, canActivate: [authGuard]},
  {path: 'selfrating', component: SelfRatingComponent, canActivate: [authGuard]},
  {path: '**', component: LoginComponent, canActivate: [authGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
