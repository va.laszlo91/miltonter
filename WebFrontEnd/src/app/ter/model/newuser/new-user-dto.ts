export interface NewUserDto {
  name: string,
  email: string,
  number: number,
  dept: string,
  address: string,
  birthday: string,
  terStatus: string,
  position: string,
  userName: string,
  password: string
}
