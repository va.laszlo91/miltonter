package com.example.webbackend.ter.modules.terlist;

import com.example.webbackend.ter.model.terddata.TerDataDto;
import com.example.webbackend.ter.tables.terdata.TerDataRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TerListService {

    @Autowired
    TerDataRepository terDataRepository;

    ModelMapper modelMapper = new ModelMapper();

    public List<TerDataDto> getAllTerData() {
        System.out.println(terDataRepository.findAll());
        return mapList(terDataRepository.findAll(), TerDataDto.class);
    }


    <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }
}
