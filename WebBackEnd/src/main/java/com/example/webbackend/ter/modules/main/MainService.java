package com.example.webbackend.ter.modules.main;

import com.example.webbackend.ter.model.terddata.TerDataDto;
import com.example.webbackend.ter.tables.terdata.TerDataRepository;
import com.example.webbackend.ter.tables.user.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MainService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TerDataRepository terDataRepository;

    ModelMapper modelMapper = new ModelMapper();

    public String getTerStatus(String name) {
        System.out.println(userRepository.getStatusTer(name));
        return userRepository.getStatusTer(name);
    }

    public TerDataDto getTanarTerData(String userName) {
        return modelMapper.map(terDataRepository.getTanarTerData(userName), TerDataDto.class);
    }

    public Boolean checkReadyStatusTerData() {
        System.out.println(terDataRepository.checkReadyStatus());
        if (terDataRepository.checkReadyStatus()
                .isEmpty()) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

}
