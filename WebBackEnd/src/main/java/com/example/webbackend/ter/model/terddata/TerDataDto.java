package com.example.webbackend.ter.model.terddata;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TerDataDto {

    private int id;
    private String name;
    private String userName;
    private String pub;
    private String pubValue;
    private String teach;
    private String teachValue;
    private String search;
    private String searchValue;
    private String plusz;
    private String pluszValue;
    private String status;
}
