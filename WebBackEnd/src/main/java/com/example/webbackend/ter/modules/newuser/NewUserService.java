package com.example.webbackend.ter.modules.newuser;

import com.example.webbackend.ter.model.newuser.NewUserDto;
import com.example.webbackend.ter.tables.login.LoginDomain;
import com.example.webbackend.ter.tables.login.LoginRepository;
import com.example.webbackend.ter.tables.user.UserDomain;
import com.example.webbackend.ter.tables.user.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NewUserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    LoginRepository loginRepository;

    ModelMapper modelMapper = new ModelMapper();


    public Boolean updateUser(NewUserDto newUserDto) {
        try {
            UserDomain userDomain = new UserDomain();
            userDomain.setName(newUserDto.getName());
            userDomain.setUserName(newUserDto.getUserName());
            userDomain.setEmail(newUserDto.getEmail());
            userDomain.setNumber(newUserDto.getNumber());
            userDomain.setAddress(newUserDto.getAddress());
            userDomain.setBirthday(newUserDto.getBirthday());
            userDomain.setTerStatus(newUserDto.getTerStatus());
            userDomain.setPosition(newUserDto.getPosition());
            userDomain.setDept(newUserDto.getDept());
            userRepository.saveAndFlush(userDomain);
            LoginDomain loginDomain = new LoginDomain();
            loginDomain.setName(newUserDto.getName());
            loginDomain.setUserName(newUserDto.getUserName());
            loginDomain.setRoles(newUserDto.getPosition());
            loginDomain.setPassword(newUserDto.getPassword());
            loginRepository.saveAndFlush(loginDomain);
            return Boolean.TRUE;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }
}
