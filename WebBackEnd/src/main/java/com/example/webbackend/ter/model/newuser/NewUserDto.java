package com.example.webbackend.ter.model.newuser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewUserDto {

    private int id;
    private String name;
    private String email;
    private int number;
    private String dept;
    private String address;
    private String birthday;
    private String terStatus;
    private String position;
    private String userName;
    private String password;
}