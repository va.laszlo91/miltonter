//@ts-nocheck
import {Component, OnInit} from '@angular/core';
import {ConfirmationService, MessageService} from "primeng/api";
import {TerService} from "./ter.service";
import {TerInfoDto} from "../../model/ter/ter-info-dto";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TerDataDto} from "../../model/terdata/ter-data-dto";

interface ExportColumn {
  title: string;
  dataKey: string;
}

interface Column {
  field: string;
  header: string;
  customExportHeader?: string;
}

@Component({
  selector: 'app-ter',
  templateUrl: './ter.component.html',
  styleUrl: './ter.component.css',
  providers: [MessageService, ConfirmationService]
})
export class TerComponent implements OnInit {

  constructor(
    private readonly terService: TerService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
  ) {
  }

  myForm!: FormGroup;

  totalIndex = 0;

  terTasks: TerInfoDto[] = [];
  terDataDtos!: TerDataDto[];
  terStatus = "";

  pubList: string [] = []
  teach: string [] = []
  search: string [] = []

  tooMuchTotalindex = false;


  ngOnInit(): void {
    this.getAllTerInfo();
    this.creatLoginForm();
    this.getUserInfo();
    this.cols = [
      {field: 'code', header: 'Code', customExportHeader: 'Product Code'},
      {field: 'name', header: 'Name'},
      {field: 'category', header: 'Category'},
      {field: 'quantity', header: 'Quantity'}
    ];
  }

  getAllTerInfo() {
    console.log("getAllTerInfo")
    this.terService.getAllTerInfo().subscribe({
      next: (response) => {
        for (const item of response) {
          this.terTasks.push(item);
          switch (item.type) {
            case 'PUB':
              this.pubList.push(item.task)
              break;
            case 'TEACH':
              this.teach.push(item.task)
              break;
            default:
              this.search.push(item.task)
              break;
          }
        }
      },
      error: error => console.log(error)
    })
    console.log("getAllTerInfo VÉGE")
  }

  creatLoginForm() {
    console.log("creatLoginForm")
    this.myForm = new FormGroup({
      pub: new FormControl("", [Validators.required]),
      pubIndex: new FormControl("", [Validators.required]),
      teach: new FormControl("", [Validators.required]),
      teachIndex: new FormControl("", [Validators.required]),
      search: new FormControl("", [Validators.required]),
      searchIndex: new FormControl("", [Validators.required]),
      plus: new FormControl("", []),
      plusIndex: new FormControl("", []),
      totalInd: new FormControl(0, [Validators.max(100)])
    });
    console.log("creatLoginForm VÉGE")
  }

  insertToTerByOwn() {
    this.terService.setBody = {
      name: sessionStorage.getItem('name'),
      userName: sessionStorage.getItem('username'),
      pub: this.myForm.controls['pub'].value,
      pubValue: this.myForm.controls['pubIndex'].value,
      teach: this.myForm.controls['teach'].value,
      teachValue: this.myForm.controls['teachIndex'].value,
      search: this.myForm.controls['search'].value,
      searchValue: this.myForm.controls['searchIndex'].value,
      plusz: this.myForm.controls['plus'].value,
      pluszValue: this.myForm.controls['plusIndex'].value,
      status: 'READY'
    }
    this.terService.insertTerData().subscribe((e) => {
      if (e) {
        this.messageService.add({severity: 'success', summary: 'SIKER', detail: 'Sikeres TÉR módosítás!', life: 2000});
        this.getUserInfo();
        this.terStatus = 'ACCEPT';
      } else {
        console.log("ELBASZÓDOTT")
      }
    })
  }

  sumFormControls() {
    this.totalIndex = 0;
    for (const controlName in this.myForm.controls) {
      if (this.myForm.controls.hasOwnProperty(controlName)) {
        if (controlName.includes('Index')) {
          this.totalIndex += +this.myForm.controls[controlName].value;
        }
      }
    }
    this.myForm.controls['totalInd'].setValue(this.totalIndex);
    this.tooMuchTotalindex = (this.totalIndex > 100);
  }

  getUserInfo() {
    console.log("getUserInfo")
    this.terService.setBody = {
      param: sessionStorage.getItem('username')
    }
    this.terService.getUserTerData().subscribe({
        next: (res) => {
          console.log(res)
          if (res != "") {
            this.terDataDtos = res;
            for (const item of res) {
              if (['ACCEPT', 'READY'].includes(item.status)) {
                this.terStatus = 'ACCEPT';
              } else if (item.status === 'REJECT') {
                this.terStatus = 'NOTREADY';
                this.confirmationService.confirm({
                  header: 'Figyelem!',
                  message: 'Elutasítás miatt a vállalások kitöltése újból szükséges',
                  acceptIcon: 'pi pi-check mr-2',
                  acceptLabel: 'Korábbi adatok betöltése',
                  rejectIcon: 'pi pi-check mr-2',
                  rejectLabel: 'Nem szükséges',
                  accept: () => {
                    this.setMyFormBecauseReject(this.terDataDtos);
                  },
                  reject: () => {
                    this.myForm.markAllAsTouched();
                  }
                });
              } else {
                this.terStatus = 'NOTREADY';
              }
            }
          } else {
            this.terStatus = 'NOTREADY';
          }
          this.myForm.updateValueAndValidity();
        },
        error: (err) => {
          console.log(err)
        }
      }
    )
    console.log("getUserInfo VÉGE")

  }

  getStatuszValue(status: string) {
    switch (status) {
      case 'ACCEPT':
        return 'Elfogadva';
      case 'READY':
        return 'Elfogadásra vár';
      case 'REJECT':
        return 'Elutasítva';
    }
  }

  getStatusz(status: string) {
    switch (status) {
      case 'ACCEPT':
        return 'success';
      case 'READY':
        return 'warning';
      case 'REJECT':
        return 'danger';
    }
  }


  setMyFormBecauseReject(res: TerDataDto) {
    console.log('setMy: ')
    console.log(res)
    this.myForm.controls['pub'].setValue(res[0].pub);
    this.myForm.controls['pubIndex'].setValue(res[0].pubValue);
    this.myForm.controls['teach'].setValue(res[0].teach);
    this.myForm.controls['teachIndex'].setValue(res[0].teachValue);
    this.myForm.controls['search'].setValue(res[0].search);
    this.myForm.controls['searchIndex'].setValue(res[0].searchValue);
    this.myForm.controls['plus'].setValue(res[0].plusz);
    this.myForm.controls['plusIndex'].setValue(res[0].pluszValue);
    this.sumFormControls();
  }

  formIsValid() {
    return this.myForm.valid;
  }

  exportColumns!: ExportColumn[];
  cols!: Column[];

  /*
  exportPdf() {
    this.exportColumns = this.cols.map((col) => ({title: col.header, dataKey: col.field}));
    import('jspdf').then((jsPDF) => {
      import('jspdf-autotable').then((x) => {
        const doc = new jsPDF.default('p', 'px', 'a4');
        (doc as any).autoTable(this.exportColumns, this.terTasks);
        doc.save('products.pdf');
      });
    });
  }

  exportExcel() {
    import('xlsx').then((xlsx) => {
      const worksheet = xlsx.utils.json_to_sheet(this.products);
      const workbook = {Sheets: {data: worksheet}, SheetNames: ['data']};
      const excelBuffer: any = xlsx.write(workbook, {bookType: 'xlsx', type: 'array'});
      this.saveAsExcelFile(excelBuffer, 'products');
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }*/

  LOG() {
    this.setMyFormBecauseReject(this.terDataDtos);
  }

}
