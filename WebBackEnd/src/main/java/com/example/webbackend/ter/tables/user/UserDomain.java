package com.example.webbackend.ter.tables.user;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "USER")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "NUMBER")
    private int number;

    @Column(name = "DEPT")
    private String dept;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "BIRTHDAY")
    private String birthday;

    @Column(name = "TER_STATUS")
    private String terStatus;

    @Column(name = "POS")
    private String position;

    @Column(name = "USERNAME")
    private String userName;

}
