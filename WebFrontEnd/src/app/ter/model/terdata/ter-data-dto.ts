export interface TerDataDto {
  id: number,
  name: string
  userName: string,
  pub: string,
  pubValue: string,
  teach: string,
  teachValue: string,
  search: string,
  searchValue: string,
  plusz: string,
  pluszValue: string,
  status: string
}
