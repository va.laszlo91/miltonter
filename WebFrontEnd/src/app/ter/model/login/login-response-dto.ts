export interface LoginResponseDto{
  userName:string;
  name:string;
  roles: string;
  valid: boolean;
}
