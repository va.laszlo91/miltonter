package com.example.webbackend.ter.modules.main;

import com.example.webbackend.ter.model.common.OneParamDto;
import com.example.webbackend.ter.model.terddata.TerDataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/main")
public class MainController {


    @Autowired
    MainService mainService;

    @PostMapping("/statusz")
    public String userIsValid(
            @RequestBody OneParamDto oneParamDto
    ) {
        return mainService.getTerStatus(oneParamDto.getParam());
    }

    //TANÁR belépéskor történő TER táblában a saját TÉR státusza változott e
    @PostMapping("/tanarterdata")
    public TerDataDto getTanarTerData(
            @RequestBody OneParamDto oneParamDto
    ) {
        return mainService.getTanarTerData(oneParamDto.getParam());
    }

    //TVEZETO belépéskor történő TER táblában van e elfogadásra váró igény
    @GetMapping("/checkterdata")
    public Boolean checkReadyStatusTerData() {
        return mainService.checkReadyStatusTerData();
    }

}
