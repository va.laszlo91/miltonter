import {Component, OnInit} from '@angular/core';
import {MainService} from "./main.service";
import {ConfirmationService, MessageService} from "primeng/api";
import {Router} from "@angular/router";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrl: './main.component.css',
  providers: [MessageService, ConfirmationService]
})
export class MainComponent implements OnInit {


  constructor(
    private readonly mainService: MainService,
    private confirmationService: ConfirmationService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    if (sessionStorage.getItem('role') === 'TANAR') {
      this.getTerStatusByTeacher()
    } else if (sessionStorage.getItem('role') === 'TVEZETO') {
      this.getTerStatusByLeader()
    }
  }

  terStatus = "";

  getTerStatusByTeacher() {
    if (sessionStorage.getItem('confirmation') != 'TRUE') {
      this.mainService.setBody = {
        param: sessionStorage.getItem('name')
      }
      this.mainService.getTerStatus().subscribe(
        response => {
          this.terStatus = response;
          this.openTanarConfirmation(response)
        },
        error => console.log(error)
      )
    }
  }

  getTerStatusByLeader() {
    if (sessionStorage.getItem('confirmation') != 'TRUE') {
      this.mainService.checkReadyStatusTerData().subscribe({
          next: (response) => {
            if (response) {
              this.openTvezetConfirmation()
            }
          },
          error: (err) => {
            console.log(err)
          }
        }
      )
    }
  }

  icon = "";

  openTanarConfirmation(status: string) {
    sessionStorage.setItem('confirmation', 'TRUE')
    if (status === 'NOTREADY') {
      this.icon = 'pi pi-exclamation-circle'
      this.confirmationService.confirm({
        header: 'Figyelmeztetés',
        message: 'Még nem töltötted ki a TÉR rendszerben a vállalásaidat!',
        acceptIcon: 'pi pi-check mr-2',
        acceptLabel: 'Ugrás a TÉR-be!',
        rejectIcon: 'pi pi-times mr-2',
        rejectLabel: 'Később töltöm ki!',
        rejectButtonStyleClass: 'p-button-sm',
        acceptButtonStyleClass: 'p-button-outlined p-button-sm',
        accept: () => {
          this.router.navigate(['/ter']);
        },
        reject: () => {

        }
      });
    } else if (status === 'READY') {
      this.icon = 'pi pi-info-circle'
      this.confirmationService.confirm({
        header: 'Információ',
        message: 'Még nem fogadták el a TÉR rendszerben a vállalásaidat!',
        acceptIcon: 'pi pi-check mr-2',
        acceptLabel: 'Értettem',
        rejectVisible: false,
        accept: () => {
        }
      });
    } else if (status === 'ACCEPT') {
      this.icon = "pi pi-verified"
      this.confirmationService.confirm({
        header: 'GRATULÁLUNK!',
        message: 'Elfogadták TÉR rendszerben a vállalásaidat!',
        acceptIcon: 'pi pi-check mr-2',
        acceptLabel: 'RENDBEN',
        acceptButtonStyleClass: 'p-button-outlined p-button-sm',
        rejectVisible: false,
        accept: () => {
        }
      });
    } else if (status === 'REJECT') {
      this.icon = "pi pi-times-circle"
      this.confirmationService.confirm({
        header: 'Figyelmeztetés',
        message: 'Elutasították a TÉR rendszerben a vállalásaidat!',
        acceptIcon: 'pi pi-check mr-2',
        acceptLabel: 'Ugrás a vállalásokhoz!',
        rejectIcon: 'pi pi-times mr-2',
        rejectLabel: 'Mégsem',
        accept: () => {
          this.router.navigate(['/ter']);
          sessionStorage.setItem('confirmation', 'TRUE')
        },
        reject: () => {
          sessionStorage.setItem('confirmation', 'TRUE')
        }
      });
    }
  }

  openTvezetConfirmation() {
    sessionStorage.setItem('confirmation', 'TRUE')
    this.icon = 'pi pi-info-circle'
    this.confirmationService.confirm({
      header: 'Figyelmeztetés',
      message: 'Van elfogadásra váró tanári vállalás!',
      acceptIcon: 'pi pi-check mr-2',
      acceptLabel: 'Ugrás a vállalásokhoz!',
      rejectIcon: 'pi pi-times mr-2',
      rejectLabel: 'Később foglalkozok vele',
      accept: () => {
        this.router.navigate(['/teacherlist']);
      },
      reject: () => {
      }
    });
  }
}
