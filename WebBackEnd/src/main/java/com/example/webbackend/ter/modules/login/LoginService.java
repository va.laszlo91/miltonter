package com.example.webbackend.ter.modules.login;

import com.example.webbackend.ter.model.login.LoginDto;
import com.example.webbackend.ter.model.login.LoginResponseDto;
import com.example.webbackend.ter.tables.login.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class LoginService {


    @Autowired
    LoginRepository loginRepository;

    public LoginResponseDto userIsValid(
            @RequestBody LoginDto loginDto
    ){
        LoginResponseDto loginResponseDto = new LoginResponseDto();
        var getUserData = loginRepository.login(loginDto.getUserName(), loginDto.getPassword());
        System.out.println(loginDto);
        if(getUserData != null){
            loginResponseDto.setUserName(loginDto.getUserName());
            loginResponseDto.setValid(true);
            loginResponseDto.setRoles(getUserData.getRoles());
            loginResponseDto.setName(getUserData.getName());
            return loginResponseDto;
        }else{
            loginResponseDto.setValid(false);
            return loginResponseDto;
        }
    }
}
