import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  apiUrl = "http://localhost:8080/main";

  body: object = {};


  set setBody(v: object) {
    this.body = v;
  }

  public getTerStatus(): Observable<string> {
    return this.httpClient.post(this.apiUrl + "/statusz", this.body, {responseType: 'text'});
  }

  public checkReadyStatusTerData(): Observable<boolean> {
    return this.httpClient.get<boolean>(this.apiUrl + "/checkterdata");
  }
}
