import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TerDataDto} from "../../model/terdata/ter-data-dto";

@Injectable({
  providedIn: 'root'
})
export class TerListService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  apiUrl = "http://localhost:8080/terlist";

  body: object = {};

  set setBody(v: object) {
    this.body = v;
  }

  public getAllTerData(): Observable<TerDataDto []> {
    return this.httpClient.get<TerDataDto []>(this.apiUrl + "/getallinfo");
  }
}
