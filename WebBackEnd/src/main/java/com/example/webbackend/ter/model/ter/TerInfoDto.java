package com.example.webbackend.ter.model.ter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TerInfoDto {

    private int id;
    private String task;
    private String type;
}
