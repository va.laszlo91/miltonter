import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NewUserService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  apiUrl = "http://localhost:8080/newuser";

  body: object = {};

  set setBody(v: object) {
    this.body = v;
  }

  public insertNewUser(): Observable<boolean> {
    return this.httpClient.post<boolean>(this.apiUrl + "/insert", this.body);
  }
}
