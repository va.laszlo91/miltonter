import {Component, OnInit} from '@angular/core';
import {LoginService} from "./login.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MessageService} from "primeng/api";
import {Router, RouterLink, RouterModule} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
  providers: [MessageService]
})
export class LoginComponent implements OnInit{


  constructor(
    private readonly loginService: LoginService,
    private messageService: MessageService,
    private router: Router
  ) {}


  ngOnInit(): void {
    this.creatLoginForm();
  }

  loginForm!: FormGroup;

  creatLoginForm(){
    this.loginForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required])
    });
  }


  checkUser(){
    this.loginService.setBody = {
      userName: this.loginForm.value.name,
      password: this.loginForm.value.password
    }
    this.loginService.checkUser().subscribe(
      response => {
          if(response.valid){
            console.log(response)
            this.router.navigate(['/main']);
            sessionStorage.setItem('username', response.userName);
            sessionStorage.setItem('role', response.roles);
            sessionStorage.setItem('name', response.name)
            console.log("response.name: " + response.name)
          }else{
            this.messageService.add({ severity: 'error', summary: 'HIBA', detail: 'Hibás felhasználónév vagy jelszó!', life: 2000 });
        }
      },
      error => console.log(error)
    )
  }

  get name() {
    return this.loginForm.controls['name'];
  }

  get password() {
    return this.loginForm.controls['password'];
  }

}
