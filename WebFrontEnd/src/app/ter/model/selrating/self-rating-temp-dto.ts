export interface SelfRatingTempDto {
  id: number,
  name: string,
  userName: string,
  status: string,
  srsz1: number;
  srsz2: number;
  srsz3: number;
  srsz4: number;
  srsz5: number;
  srsz6: number;
  srsz7: number;
  srsz8: number;
}
