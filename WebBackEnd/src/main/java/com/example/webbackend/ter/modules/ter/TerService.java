package com.example.webbackend.ter.modules.ter;

import com.example.webbackend.ter.model.ter.TerInfoDto;
import com.example.webbackend.ter.model.terddata.TerDataDto;
import com.example.webbackend.ter.tables.terdata.TerDataDomain;
import com.example.webbackend.ter.tables.terdata.TerDataRepository;
import com.example.webbackend.ter.tables.terinfo.TerInfoRepository;
import com.example.webbackend.ter.tables.user.UserDomain;
import com.example.webbackend.ter.tables.user.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TerService {

    @Autowired
    TerInfoRepository terInfoRepository;

    @Autowired
    TerDataRepository terDataRepository;

    @Autowired
    UserRepository userRepository;

    ModelMapper modelMapper = new ModelMapper();

    public List<TerInfoDto> getAllTerInfoData() {
        return mapList(terInfoRepository.findAll(), TerInfoDto.class);
    }


    public Boolean insertTanarTerDataByOwn(TerDataDto terDataDto) {
        try {
            UserDomain userDomain = modelMapper.map(userRepository.getUserByUserName(terDataDto.getUserName()), UserDomain.class);
            userDomain.setTerStatus(terDataDto.getStatus());
            userRepository.saveAndFlush(userDomain);
            if (terDataRepository.getTanarId(terDataDto.getUserName()) != null) {
                terDataDto.setId(terDataRepository.getTanarId(terDataDto.getUserName()));
            }
            TerDataDomain terDataDomain = modelMapper.map(terDataDto, TerDataDomain.class);
            terDataRepository.saveAndFlush(terDataDomain);
            return Boolean.TRUE;
        } catch (Exception e) {
            System.out.println(e);
            return Boolean.FALSE;
        }
    }

    public List<TerDataDto> getOneTerDataByUserName(String userName) {
        return mapList(terDataRepository.getOneTerDataByUserName(userName), TerDataDto.class);
    }

    <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }
}
