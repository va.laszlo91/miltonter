import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './ter/components/header/header.component';
import {LoginComponent} from './ter/components/login/login.component';
import {MainComponent} from './ter/components/main/main.component';
import {UserComponent} from './ter/components/user/user.component';
import {BadgeModule} from "primeng/badge";
import {AvatarGroupModule} from "primeng/avatargroup";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ButtonModule} from "primeng/button";
import {CommonModule, DatePipe, NgOptimizedImage} from "@angular/common";
import {InputTextModule} from "primeng/inputtext";
import {SidebarModule} from "primeng/sidebar";
import {AvatarModule} from "primeng/avatar";
import {StyleClassModule} from "primeng/styleclass";
import {RippleModule} from "primeng/ripple";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {ToastModule} from "primeng/toast";
import {PasswordModule} from "primeng/password";
import {DividerModule} from "primeng/divider";
import {CardModule} from "primeng/card";
import {TerComponent} from './ter/components/ter/ter.component';
import {TableModule} from "primeng/table";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {RadioButtonModule} from "primeng/radiobutton";
import {InputNumberModule} from "primeng/inputnumber";
import {TagModule} from "primeng/tag";
import {DialogModule} from "primeng/dialog";
import {KeyFilterModule} from "primeng/keyfilter";
import {DropdownModule} from "primeng/dropdown";
import {TerListComponent} from './ter/components/ter-list/ter-list.component';
import {InputMaskModule} from "primeng/inputmask";
import {NewUserComponent} from './ter/components/new-user/new-user.component';
import {CalendarModule} from "primeng/calendar";
import { SelfRatingComponent } from './ter/components/self-rating/self-rating.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    MainComponent,
    UserComponent,
    TerComponent,
    TerListComponent,
    NewUserComponent,
    SelfRatingComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ButtonModule,
    NgOptimizedImage,
    InputTextModule,
    SidebarModule,
    AvatarModule,
    StyleClassModule,
    RippleModule,
    AvatarGroupModule,
    BadgeModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastModule,
    PasswordModule,
    DividerModule,
    CardModule,
    TableModule,
    ConfirmDialogModule,
    RadioButtonModule,
    InputNumberModule,
    TagModule,
    DialogModule,
    KeyFilterModule,
    FormsModule,
    DropdownModule,
    InputMaskModule,
    CalendarModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {
}
