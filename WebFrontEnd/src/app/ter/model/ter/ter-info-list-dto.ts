import {TerInfoDto} from "./ter-info-dto";

export interface TerInfoListDto{

  terInfoListDto: Array<TerInfoDto>
}
