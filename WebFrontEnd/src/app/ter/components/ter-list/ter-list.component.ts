//@ts-nocheck

import {Component, OnInit} from '@angular/core';
import {ConfirmationService, MessageService} from "primeng/api";
import {TerListService} from "./ter-list.service";
import {TerDataDto} from "../../model/terdata/ter-data-dto";
import {TerService} from "../ter/ter.service";

@Component({
  selector: 'app-ter-list',
  templateUrl: './ter-list.component.html',
  styleUrl: './ter-list.component.css',
  providers: [MessageService, ConfirmationService]
})
export class TerListComponent implements OnInit {


  constructor(
    private readonly terListService: TerListService,
    private readonly terService: TerService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
  }

  terDataDtos!: TerDataDto[];

  ngOnInit(): void {
    this.getAllTerDataInfo();
  }

  getAllTerDataInfo() {
    this.terListService.getAllTerData().subscribe({
      next: (response) => {
        this.terDataDtos = response;
      },
      error: error => console.log(error)
    })
  }

  getStatusz(status: string) {
    switch (status) {
      case 'ACCEPT':
        return 'success';
      case 'READY':
        return 'warning';
      case 'REJECT':
        return 'danger';
    }
  }

  getStatuszValue(status: string) {
    switch (status) {
      case 'ACCEPT':
        return 'Elfogadva';
      case 'READY':
        return 'Elfogadásra vár';
      case 'REJECT':
        return 'Elutasítva';
    }
  }

  selectAction(terDataDto: TerDataDto, action: string) {
    let newStatus = action === 'accept' ? 'ACCEPT' : 'REJECT';
    console.log('newStatus:' + newStatus)
    this.terService.setBody = {
      id: terDataDto.id,
      name: terDataDto.name,
      userName: terDataDto.userName,
      pub: terDataDto.pub,
      pubValue: terDataDto.pubValue,
      teach: terDataDto.teach,
      teachValue: terDataDto.teachValue,
      search: terDataDto.search,
      searchValue: terDataDto.searchValue,
      plusz: terDataDto.plusz,
      pluszValue: terDataDto.pluszValue,
      status: newStatus
    }
    this.terService.insertTerData().subscribe({
      next: (response) => {
        console.log(response);
        this.getAllTerDataInfo();
      },
      error: (err) => {
        console.log(err)
      }
    })
  }
}
