//@ts-nocheck
import {Component, OnInit} from '@angular/core';
import {MainService} from "../main/main.service";
import {ConfirmationService, MessageService} from "primeng/api";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SelfRatingService} from "./self-rating.service";
import {SelfRatingTempDto} from "../../model/selrating/self-rating-temp-dto";

@Component({
  selector: 'app-self-rating',
  templateUrl: './self-rating.component.html',
  styleUrl: './self-rating.component.css',
  providers: [MessageService, ConfirmationService]
})
export class SelfRatingComponent implements OnInit {


  constructor(
    private readonly mainService: MainService,
    private readonly selfRatingService: SelfRatingService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private router: Router
  ) {
  }

  userForm!: FormGroup;

  allSelfRatingTemp!: SelfRatingTempDto[];

  icon = "";
  terStatus = "";
  szempontArray = [
    {name: "Pedagógia munka minősége", value: 28, formControlName: "pedagogia"},
    {name: "Feladatvállalás mennyiségi mutatói", value: 12, formControlName: "feladatVallalas"},
    {name: "Munkavégzés megbízhatósága, határidők betartása", value: 8, formControlName: "munkaVegzes"},
    {name: "Kommunikáció, együttműködés", value: 6, formControlName: "kommunikacio"},
    {name: "Tehetséggondozás, felzárkóztatás", value: 8, formControlName: "tehetseg"},
    {name: "Motiváció, elkötelezettség, etikus magatartás", value: 6, formControlName: "motivacio"},
    {name: "Egyedi intézményi értékelési szempont", value: 8, formControlName: "egyedi"},
    {name: "Három, személyre szabott teljesítménycél", value: 24, formControlName: "harom"}
  ]
  totalIndex = 0;
  tvezeto = false;
  dialogVisible: boolean = false;
  acceptedDialogVisible: boolean = false;
  chooseUser: [];
  chooseAcceptedUser: [];
  selectedId: number;
  selectedTeacher = "";
  selectedUserName = "";
  title = "";
  secondTitle = "";

  ngOnInit(): void {
    this.createUserForm();
    if (sessionStorage.getItem('role') === 'TANAR') {
      this.getTerStatusByTeacher()
      this.title = "Önértékelés"
    } else if (sessionStorage.getItem('role') === 'TVEZETO') {
      this.getAllTempData();
      this.title = "Önértékelések kezelése"
      this.secondTitle = "Elfogadott Önértékelések"
      this.tvezeto = true;
    }
  }


  createUserForm() {
    this.userForm = new FormGroup({
      pedagogia: new FormControl("", [Validators.max(28)]),
      feladatVallalas: new FormControl("", [Validators.max(12)]),
      munkaVegzes: new FormControl("", [Validators.max(8)]),
      kommunikacio: new FormControl("", [Validators.max(6)]),
      tehetseg: new FormControl("", [Validators.max(8)]),
      motivacio: new FormControl("", [Validators.max(6)]),
      egyedi: new FormControl("", [Validators.max(8)]),
      harom: new FormControl("", [Validators.max(24), Validators.required]),
    });
  }

  //TANÁR: Amíg nem fogadták el a vállalásokat, nem lehet kitölteni az Önértékelést!
  getTerStatusByTeacher() {
    this.mainService.setBody = {
      param: sessionStorage.getItem('name')
    }
    this.mainService.getTerStatus().subscribe(
      response => {
        //response => TER statusa
        if (response != 'ACCEPT') {
          this.notAccepTerConfirmation();
        } else {
          this.checkSelfRatingStatus(response);
        }
      },
      error => console.log(error)
    )
  }

  //TANÁR: SelfRatingTemp táblába bekérdezés,hogy beküldte e már vagy sem az Önértékelést
  checkSelfRatingStatus(resp: string) {
    this.selfRatingService.setBody = {
      param: sessionStorage.getItem('name')
    }
    this.selfRatingService.getSelRatingStatus().subscribe({
      next: (res) => {

        if (res === 'READY') {
          //Leadta már az Önértékelést, várnia kell üzenet!
          this.selfRatingIsDoneConfirmation();
        } else {
          //Nem adta még le, így megjelenik a kitöltő oldal!
          this.terStatus = resp;
        }
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  //TANÁR: Önértékelés sikeresen leadva, de várnia kell a Tvezetőre Confirmation ablak
  selfRatingIsDoneConfirmation() {
    this.icon = 'pi pi-exclamation-circle'
    this.confirmationService.confirm({
      header: 'Figyelmeztetés',
      message: 'Már leadta az Önértékelést! Várjon míg a felettese keresi Önt ez ügyben!',
      rejectLabel: 'Ugrás a főoldalra!',
      rejectButtonStyleClass: 'p-button-sm',
      acceptVisible: false,
      reject: () => {
        this.router.navigate(['/main']);
      }
    });
  }

  //TANÁR: Nem fogadták még el a vállalásait Confirmation ablak
  notAccepTerConfirmation() {
    this.icon = 'pi pi-exclamation-circle'
    this.confirmationService.confirm({
      header: 'Figyelmeztetés',
      message: 'Amíg nem fogadták el a vállalásaidat, addig nem lehet az Önértékelést elkezdeni!',
      acceptLabel: 'Ugrás a TÉR-be!',
      rejectLabel: 'Ugrás a főoldalra!',
      rejectButtonStyleClass: 'p-button-sm',
      acceptButtonStyleClass: 'p-button-sm',
      accept: () => {
        this.router.navigate(['/ter']);
      },
      reject: () => {
        this.router.navigate(['/main']);
      }
    });
  }

  //TANÁR: Sikeresen leadta az Önértékelést Confirmation ablak
  selfRatingIsInsertByTeacherConfirmation() {
    this.icon = 'pi pi-exclamation-circle'
    this.confirmationService.confirm({
      header: 'Köszönjük',
      message: 'Az Önértékelést sikeresen leadta. Várjon míg a felettese jelentkezni fog ez ügyben!',
      rejectLabel: 'Ugrás a főoldalra!',
      rejectButtonStyleClass: 'p-button-sm',
      acceptVisible: false,
      reject: () => {
        this.router.navigate(['/main']);
      }
    });
  }

  //TVEZETO: Sikeresen leadta a választott Tanár Önértékelését Confirmation ablak
  selfRatingIsInsertByLeaderConfirmation(name: string) {
    this.icon = 'pi pi-exclamation-circle'
    this.confirmationService.confirm({
      header: 'Köszönjük',
      message: 'A(z) ' + name + ' adatai sikeresen rögzítésre kerültek!',
      rejectLabel: 'Folytatás',
      rejectButtonStyleClass: 'p-button-sm',
      acceptLabel: 'Ugrás a főoldalra!',
      acceptButtonStyleClass: 'p-button-sm',
      accept: () => {
        this.router.navigate(['/main']);
      },
      reject: () => {

      }
    });
  }


  sumFormControls() {
    this.totalIndex = 0;
    for (const controlName in this.userForm.controls) {
      if (this.userForm.controls.hasOwnProperty(controlName)) {
        this.totalIndex += +this.userForm.controls[controlName].value;

      }
    }
    //this.tooMuchTotalindex = (this.totalIndex > 100);
  }

  insertToTemp() {
    this.selfRatingService.setBody = {
      name: sessionStorage.getItem('name'),
      userName: sessionStorage.getItem('username'),
      status: 'READY',
      srsz1: +this.userForm.controls['pedagogia'].value,
      srsz2: +this.userForm.controls['feladatVallalas'].value,
      srsz3: +this.userForm.controls['munkaVegzes'].value,
      srsz4: +this.userForm.controls['kommunikacio'].value,
      srsz5: +this.userForm.controls['tehetseg'].value,
      srsz6: +this.userForm.controls['motivacio'].value,
      srsz7: +this.userForm.controls['egyedi'].value,
      srsz8: +this.userForm.controls['harom'].value,
    }
    this.selfRatingService.insertToTemp().subscribe((e) => {
      if (e) {
        this.selfRatingIsInsertByTeacherConfirmation();
      } else {
        console.log("ELBASZÓDOTT")
      }
    })
  }

  getAllTempData() {
    this.selfRatingService.getAllTempData().subscribe({
      next: (response) => {
        this.allSelfRatingTemp = response;
        console.log(response)
      },
      error: error => console.log(error)
    })
  }

  getStatusz(status: string) {
    switch (status) {
      case 'ACCEPT':
        return 'success';
      case 'READY':
        return 'warning';
      case 'REJECT':
        return 'danger';
    }
  }

  getStatuszValue(status: string) {
    switch (status) {
      case 'ACCEPT':
        return 'Elfogadva';
      case 'READY':
        return 'Elfogadásra vár';
      case 'REJECT':
        return 'Elutasítva';
    }
  }

  showDialog(dto: SelfRatingTempDto) {
    this.userForm.reset();
    this.chooseUser = [];
    this.selectedId = dto.id;
    this.selectedTeacher = dto.name;
    this.selectedUserName = dto.userName;
    this.chooseUser = [
      {name: "Pedagógia munka minősége", value: 28, tValue: dto.srsz1, formControlName: "pedagogia"},
      {name: "Feladatvállalás mennyiségi mutatói", value: 12, tValue: dto.srsz2, formControlName: "feladatVallalas"},
      {
        name: "Munkavégzés megbízhatósága, határidők betartása",
        value: 8,
        tValue: dto.srsz3,
        formControlName: "munkaVegzes"
      },
      {name: "Kommunikáció, együttműködés", value: 6, tValue: dto.srsz4, formControlName: "kommunikacio"},
      {name: "Tehetséggondozás, felzárkóztatás", value: 8, tValue: dto.srsz5, formControlName: "tehetseg"},
      {
        name: "Motiváció, elkötelezettség, etikus magatartás",
        value: 6,
        tValue: dto.srsz6,
        formControlName: "motivacio"
      },
      {name: "Egyedi intézményi értékelési szempont", value: 8, tValue: dto.srsz7, formControlName: "egyedi"},
      {name: "Három, személyre szabott teljesítménycél", value: 24, tValue: dto.srsz8, formControlName: "harom"}
    ]
    this.dialogVisible = true;
    console.log(dto)
  }

  insertToFinal() {
    this.selfRatingService.setBody = {
      id: this.selectedId,
      name: this.selectedTeacher,
      userName: this.selectedUserName,
      status: 'ACCEPT',
      srsz1: +this.userForm.controls['pedagogia'].value,
      srsz2: +this.userForm.controls['feladatVallalas'].value,
      srsz3: +this.userForm.controls['munkaVegzes'].value,
      srsz4: +this.userForm.controls['kommunikacio'].value,
      srsz5: +this.userForm.controls['tehetseg'].value,
      srsz6: +this.userForm.controls['motivacio'].value,
      srsz7: +this.userForm.controls['egyedi'].value,
      srsz8: +this.userForm.controls['harom'].value,
    }
    this.selfRatingService.insertToTemp().subscribe((e) => {
      if (e) {
        this.selfRatingIsInsertByLeaderConfirmation(this.selectedTeacher);
      } else {
        console.log("ELBASZÓDOTT")
      }
    })
  }

  showAcceptedDialog(dto: SelfRatingTempDto) {
    this.userForm.reset();
    this.chooseUser = [];
    this.selectedId = dto.id;
    this.selectedTeacher = dto.name;
    this.selectedUserName = dto.userName;
    this.chooseAcceptedUser = [
      {name: "Pedagógia munka minősége", value: dto.srsz1},
      {name: "Feladatvállalás mennyiségi mutatói", value: dto.srsz2},
      {
        name: "Munkavégzés megbízhatósága, határidők betartása",
        value: dto.srsz3,
      },
      {name: "Kommunikáció, együttműködés", value: dto.srsz4},
      {name: "Tehetséggondozás, felzárkóztatás", value: dto.srsz5},
      {
        name: "Motiváció, elkötelezettség, etikus magatartás",
        value: dto.srsz6
      },
      {name: "Egyedi intézményi értékelési szempont", value: dto.srsz7},
      {name: "Három, személyre szabott teljesítménycél", value: dto.srsz8}
    ]
    this.acceptedDialogVisible = true;
    console.log(dto)
  }

  LOG() {
    console.log(this.userForm.controls)
  }
}
