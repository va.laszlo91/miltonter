import {Component, OnInit} from '@angular/core';
import {MessageService} from "primeng/api";
import {UserService} from "./user.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {UserDto} from "../../model/user/user-dto";
import {DatePipe, registerLocaleData} from "@angular/common";
import localeHu from '@angular/common/locales/hu'

registerLocaleData(localeHu)

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.css',
  providers: [MessageService]
})
export class UserComponent implements OnInit {


  constructor(
    private readonly userService: UserService,
    private messageService: MessageService,
    private readonly datePipe: DatePipe
  ) {
  }

  ngOnInit(): void {
    this.createUserForm();
    this.getUserInfo();
  }

  userForm!: FormGroup;
  userDataSource!: UserDto;

  createUserForm() {
    this.userForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      userName: new FormControl("", []),
      email: new FormControl("", [Validators.required]),
      number: new FormControl("", [Validators.required]),
      dept: new FormControl("", [Validators.required]),
      birthday: new FormControl("", [Validators.required]),
      position: new FormControl("", [Validators.required]),
      irsz: new FormControl("", [Validators.required]),
      telepules: new FormControl("", [Validators.required]),
      kozteruletNeve: new FormControl("", [Validators.required]),
      kozteruletJellege: new FormControl("", [Validators.required]),
      hazszam: new FormControl("", [Validators.required]),
    });
  }

  getUserInfo() {
    this.userService.setBody = {
      param: sessionStorage.getItem('name')
    }
    this.userService.getUserData().subscribe(
      response => {
        if (response) {
          this.userDataSource = response;
          this.setUserForm();
          this.userForm.disable();
        } else {
          console.log("error")
        }
      },
      error => console.log(error)
    )
  }

  setUserForm() {
    this.userForm.controls['name'].setValue(this.userDataSource.name);
    this.userForm.controls['email'].setValue(this.userDataSource.email);
    this.userForm.controls['number'].setValue(this.userDataSource.number);
    this.userForm.controls['dept'].setValue(this.userDataSource.dept);
    this.userForm.controls['birthday'].setValue(this.userDataSource.birthday.replaceAll("-", "."));
    this.userForm.controls['userName'].setValue(this.userDataSource.userName);
    this.userForm.controls['position'].setValue(this.userDataSource.position);
    let addressArray = this.splitStringBySlash(this.userDataSource.address)
    this.userForm.controls['irsz'].setValue(addressArray[0]);
    this.userForm.controls['telepules'].setValue(addressArray[1]);
    this.userForm.controls['kozteruletNeve'].setValue(addressArray[2]);
    this.userForm.controls['kozteruletJellege'].setValue(addressArray[3]);
    this.userForm.controls['hazszam'].setValue(addressArray[4]);

  }

  splitStringBySlash(str: string): string[] {
    return str.split("/");
  }

  enableUserForm() {
    this.userForm.controls['number'].enable();
    this.userForm.controls['birthday'].enable();
    this.userForm.controls['position'].setValue(this.userDataSource.position);
    this.userForm.controls['irsz'].enable();
    this.userForm.controls['telepules'].enable();
    this.userForm.controls['kozteruletNeve'].enable();
    this.userForm.controls['kozteruletJellege'].enable();
    this.userForm.controls['hazszam'].enable();
  }

  insertData() {
    console.log(this.userForm.controls['birthday'].value);
    this.userService.setBody = {
      id: this.userDataSource.id,
      name: this.userForm.controls['name'].value.toUpperCase(),
      email: this.userForm.controls['email'].value.toUpperCase(),
      number: this.userForm.controls['number'].value,
      dept: this.userForm.controls['dept'].value.toUpperCase(),
      terStatus: this.userDataSource.terStatus.toUpperCase(),
      userName: this.userDataSource.userName.toUpperCase(),
      position: this.userForm.controls['position'].value.toUpperCase(),
      birthday: this.datePipe.transform(this.userForm.controls['birthday'].value, "yyyy-MM-dd"),
      address: this.userForm.controls['irsz'].value.toUpperCase() + "/" + this.userForm.controls['telepules'].value.toUpperCase() + "/" + this.userForm.controls['kozteruletNeve'].value.toUpperCase() + "/" + this.userForm.controls['kozteruletJellege'].value.toUpperCase() + "/" + this.userForm.controls['hazszam'].value.toUpperCase()
    }
    this.userService.insertNewUserData().subscribe((e) => {
      if (e) {
        this.messageService.add({severity: 'success', summary: 'SIKER', detail: 'Sikeres adat módosítás!', life: 2000});
        this.userForm.disable();
      } else {
        console.log("ELBASZÓDOTT")
      }
    })
  }

}
