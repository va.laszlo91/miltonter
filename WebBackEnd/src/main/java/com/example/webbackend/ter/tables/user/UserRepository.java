package com.example.webbackend.ter.tables.user;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepositoryImplementation<UserDomain, Integer> {


    @Query("from UserDomain a "
            + "where a.name = :name")
    UserDomain getUserByName(
            @Param("name") String name
    );


    @Query("select a.terStatus from UserDomain a "
            + "where a.name = :name")
    String getStatusTer(
            @Param("name") String name
    );

    @Query("from UserDomain a "
            + "where a.userName = :userName")
    UserDomain getUserByUserName(
            @Param("userName") String userName
    );

}
