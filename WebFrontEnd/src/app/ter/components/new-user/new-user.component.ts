import {Component, OnInit} from '@angular/core';
import {MessageService} from "primeng/api";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NewUserService} from "./new-user.service";
import {DatePipe, registerLocaleData} from "@angular/common";
import localeHu from '@angular/common/locales/hu'

registerLocaleData(localeHu)

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrl: './new-user.component.css',
  providers: [MessageService]
})
export class NewUserComponent implements OnInit {

  constructor(
    private readonly newUserService: NewUserService,
    private messageService: MessageService,
    private readonly datePipe: DatePipe
  ) {
  }

  ngOnInit(): void {
    this.createUserForm();
  }

  myForm!: FormGroup;


  createUserForm() {
    this.myForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required]),
      number: new FormControl("", [Validators.required]),
      dept: new FormControl("", [Validators.required]),
      birthday: new FormControl("", [Validators.required]),
      irsz: new FormControl("", [Validators.required]),
      telepules: new FormControl("", [Validators.required]),
      kozteruletNeve: new FormControl("", [Validators.required]),
      kozteruletJellege: new FormControl("", [Validators.required]),
      hazszam: new FormControl("", [Validators.required]),
      pos: new FormControl("", [Validators.required]),
      userName: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
    });
  }

  insertData() {
    this.newUserService.setBody = {
      name: this.myForm.controls['name'].value.toUpperCase(),
      email: this.myForm.controls['email'].value.toUpperCase(),
      number: this.myForm.controls['number'].value,
      dept: this.myForm.controls['dept'].value.toUpperCase(),
      address: this.myForm.controls['irsz'].value.toUpperCase() + "/" + this.myForm.controls['telepules'].value.toUpperCase() + "/" + this.myForm.controls['kozteruletNeve'].value.toUpperCase() + "/" + this.myForm.controls['kozteruletJellege'].value.toUpperCase() + "/" + this.myForm.controls['hazszam'].value.toUpperCase(),
      birthday: this.datePipe.transform(this.myForm.controls['birthday'].value, "yyyy-MM-dd"),
      terStatus: 'NOTREADY',
      position: this.myForm.controls['pos'].value.toUpperCase(),
      userName: this.myForm.controls['userName'].value,
      password: this.myForm.controls['password'].value
    }
    this.newUserService.insertNewUser().subscribe((e) => {
      if (e) {
        this.messageService.add({severity: 'success', summary: 'SIKER', detail: 'Sikeres rögzítés!', life: 2000});
        this.myForm.reset();
      } else {
        console.log("ELBASZÓDOTT")
      }
    })
  }
}
