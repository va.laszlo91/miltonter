package com.example.webbackend.ter.tables.terdata;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TER_DATA")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TerDataDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "PUB")
    private String pub;

    @Column(name = "PUBVALUE")
    private String pubValue;

    @Column(name = "TEACH")
    private String teach;

    @Column(name = "TEACHVALUE")
    private String teachValue;

    @Column(name = "SEARCH")
    private String search;

    @Column(name = "SEARCHVALUE")
    private String searchValue;

    @Column(name = "PLUSZ")
    private String plusz;

    @Column(name = "PLUSZVALUE")
    private String pluszValue;

    @Column(name = "STATUS")
    private String status;
}
