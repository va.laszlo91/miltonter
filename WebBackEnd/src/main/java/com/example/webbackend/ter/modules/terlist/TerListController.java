package com.example.webbackend.ter.modules.terlist;

import com.example.webbackend.ter.model.terddata.TerDataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/terlist")
public class TerListController {

    @Autowired
    TerListService terListService;

    @GetMapping("/getallinfo")
    public List<TerDataDto> getAllTerInfo() {
        return terListService.getAllTerData();
    }
}
