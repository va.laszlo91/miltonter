package com.example.webbackend.ter.model.selfrating;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SelfRatingTempDto {

    private int id;
    private String name;
    private String userName;
    private String status;
    private int srsz1;
    private int srsz2;
    private int srsz3;
    private int srsz4;
    private int srsz5;
    private int srsz6;
    private int srsz7;
    private int srsz8;
}
