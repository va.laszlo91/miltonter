package com.example.webbackend.ter.tables.terdata;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TerDataRepository extends JpaRepositoryImplementation<TerDataDomain, Integer> {

    @Query("from TerDataDomain a "
            + "where a.userName = :userName")
    String getTanarTerData(
            @Param("userName") String userName
    );

    @Query("select a.id from TerDataDomain a "
            + "where a.userName = :userName")
    Integer getTanarId(
            @Param("userName") String userName
    );

    @Query("select a.status from TerDataDomain a "
            + "where a.status = 'READY'")
    List<String> checkReadyStatus();

    @Query("from TerDataDomain a "
            + "where a.userName = :userName")
    List<TerDataDomain> getOneTerDataByUserName(
            @Param("userName") String userName
    );
}
