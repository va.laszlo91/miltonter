import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {SelfRatingTempDto} from "../../model/selrating/self-rating-temp-dto";

@Injectable({
  providedIn: 'root'
})
export class SelfRatingService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  apiUrl = "http://localhost:8080/selfrating";

  body: object = {};

  set setBody(v: object) {
    this.body = v;
  }

  public insertToTemp(): Observable<boolean> {
    return this.httpClient.post<boolean>(this.apiUrl + '/inserttotemp', this.body);
  }

  public getAllTempData(): Observable<SelfRatingTempDto []> {
    return this.httpClient.get<SelfRatingTempDto []>(this.apiUrl + "/getalltemp");
  }

  public getSelRatingStatus(): Observable<string> {
    return this.httpClient.post(this.apiUrl + "/statusz", this.body, {responseType: 'text'});
  }

}
