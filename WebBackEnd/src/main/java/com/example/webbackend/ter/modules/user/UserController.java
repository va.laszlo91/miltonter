package com.example.webbackend.ter.modules.user;

import com.example.webbackend.ter.model.common.OneParamDto;
import com.example.webbackend.ter.model.user.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/userinfo")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/getuser")
    public UserDto getUserData(
            @RequestBody OneParamDto oneParamDto
    ) {
        return userService.getUser(oneParamDto.getParam());
    }

    @PostMapping("/insert")
    public Boolean updateUser(
            @RequestBody UserDto userDto
    ) {
        return userService.updateUser(userDto);
    }

}
