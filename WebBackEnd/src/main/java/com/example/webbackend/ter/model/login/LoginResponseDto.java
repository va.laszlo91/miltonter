package com.example.webbackend.ter.model.login;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponseDto {

    private String userName;
    private String name;
    private String roles;
    private boolean valid;
}
