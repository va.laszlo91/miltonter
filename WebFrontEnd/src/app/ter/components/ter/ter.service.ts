import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TerInfoDto} from "../../model/ter/ter-info-dto";
import {Observable} from "rxjs";
import {TerDataDto} from "../../model/terdata/ter-data-dto";

@Injectable({
  providedIn: 'root'
})
export class TerService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  apiUrl = "http://localhost:8080/ter";

  body: object = {};

  set setBody(v: object) {
    this.body = v;
  }

  public getAllTerInfo(): Observable<TerInfoDto []> {
    return this.httpClient.get<TerInfoDto []>(this.apiUrl + "/getallinfo");
  }

  public insertTerData(): Observable<boolean> {
    return this.httpClient.post<boolean>(this.apiUrl + "/insertoter", this.body);
  }

  public getUserTerData(): Observable<TerDataDto []> {
    return this.httpClient.post<TerDataDto []>(this.apiUrl + "/oneterbyuser", this.body);
  }
}
