import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserDto} from "../../model/user/user-dto";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  apiUrl = "http://localhost:8080/userinfo";

  body: object = {};


  set setBody(v: object){
    this.body = v;
  }


  public getUserData(): Observable<UserDto> {
    return this.httpClient.post<UserDto>(this.apiUrl + "/getuser",this.body);
  }

  public insertNewUserData(): Observable<boolean> {
    return this.httpClient.post<boolean>(this.apiUrl + "/insert",this.body);
  }
}
