package com.example.webbackend.ter.modules.selfrating;


import com.example.webbackend.ter.model.selfrating.SelfRatingTempDto;
import com.example.webbackend.ter.tables.selfratingfinal.SelfRatingFinalRepository;
import com.example.webbackend.ter.tables.selfratingtemp.SelfRatingTempDomain;
import com.example.webbackend.ter.tables.selfratingtemp.SelfRatingTempRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SelfRatingService {

    @Autowired
    SelfRatingTempRepository selfRatingTempRepository;

    @Autowired
    SelfRatingFinalRepository selfRatingFinalRepository;

    ModelMapper modelMapper = new ModelMapper();

    public Boolean insertDataToSelfRatingTemp(SelfRatingTempDto selRatingTempDto) {
        try {
            System.out.println(selRatingTempDto);
            SelfRatingTempDomain selfRatingTempDomain = modelMapper.map(selRatingTempDto, SelfRatingTempDomain.class);
            selfRatingTempRepository.saveAndFlush(selfRatingTempDomain);
            return Boolean.TRUE;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }


    public List<SelfRatingTempDto> getAllTempData() {
        System.out.println(selfRatingTempRepository.findAll());
        return mapList(selfRatingTempRepository.findAll(), SelfRatingTempDto.class);
    }

    public String getSelfRatingStatus(String name) {
        System.out.println(selfRatingTempRepository.getSelfRatingStatus(name));
        return selfRatingTempRepository.getSelfRatingStatus(name);
    }

    <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }
}
