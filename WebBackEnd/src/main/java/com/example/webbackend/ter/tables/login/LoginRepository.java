package com.example.webbackend.ter.tables.login;

import com.example.webbackend.ter.tables.user.UserDomain;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepositoryImplementation<LoginDomain, Integer> {

    @Query("from LoginDomain a "
            + "where a.userName = :username "
            + "and  a.password = :password")
    LoginDomain login(
            @Param("username") String username,
            @Param("password") String password
    );
}
