package com.example.webbackend.ter.tables.login;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "LOGIN")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LoginDomain {


    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ROLES")
    private String roles;

    @Column(name = "NAME")
    private String name;

}
