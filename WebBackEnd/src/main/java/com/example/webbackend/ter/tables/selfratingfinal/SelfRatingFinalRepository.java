package com.example.webbackend.ter.tables.selfratingfinal;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

@Repository
public interface SelfRatingFinalRepository extends JpaRepositoryImplementation<SelfRatingFinalDomain, Integer> {
}
