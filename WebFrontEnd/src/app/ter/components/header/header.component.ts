import {Component, OnInit, ViewChild} from '@angular/core';
import {Sidebar} from "primeng/sidebar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit {
  @ViewChild('sidebarRef') sidebarRef!: Sidebar;

  // subscription:Subscription;

  constructor(
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.setRole();
  }

  fhName!: string | null;

  sidebarVisible: boolean = false;
  loginToTvezeto: boolean = false;
  loginToTanar: boolean = false;
  loginToAdmin: boolean = false;

  setRole() {
    let role = sessionStorage.getItem('role');
    let name = sessionStorage.getItem('name');
    switch (role) {
      case 'TVEZETO':
        this.loginToTvezeto = true;
        this.fhName = name
        break;
      case 'TANAR':
        this.loginToTanar = true;
        this.fhName = name
        break;
      case 'ADMIN':
        this.loginToAdmin = true;
        this.fhName = name
        break;
      default:
        break;
    }
  }

  closeCallback(e: any): void {
    this.sidebarRef.close(e);
  }

  logout() {
    this.router.navigate(['/']);
    sessionStorage.clear();
  }

  goToMain() {
    this.router.navigate(['/main']);
    this.sidebarVisible = false;
  }

  goToUserData() {
    this.router.navigate(['/user']);
    this.sidebarVisible = false;
  }

  goToSetTEr() {
    this.router.navigate(['/ter']);
    this.sidebarVisible = false;
  }

  goToTerListByTeacher() {
    this.router.navigate(['/teacherlist']);
    this.sidebarVisible = false;
  }

  goToNewUser() {
    this.router.navigate(['/newuser']);
    this.sidebarVisible = false;
  }

  goToSelfRating() {
    this.router.navigate(['/selfrating']);
    this.sidebarVisible = false;
  }


}
