package com.example.webbackend.ter.modules.ter;

import com.example.webbackend.ter.model.common.OneParamDto;
import com.example.webbackend.ter.model.ter.TerInfoDto;
import com.example.webbackend.ter.model.terddata.TerDataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/ter")
public class TerController {


    @Autowired
    TerService terService;

    @GetMapping("/getallinfo")
    public List<TerInfoDto> getAllTerInfo() {
        return terService.getAllTerInfoData();
    }

    @PostMapping("/insertoter")
    public Boolean insertTanarTerDataByOwn(
            @RequestBody TerDataDto terDataDto
    ) {
        return terService.insertTanarTerDataByOwn(terDataDto);
    }

    @PostMapping("/oneterbyuser")
    public List<TerDataDto> getOneTerDataByUserName(
            @RequestBody OneParamDto oneParamDto
    ) {
        return terService.getOneTerDataByUserName(oneParamDto.getParam());
    }

}
