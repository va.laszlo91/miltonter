package com.example.webbackend.ter.model.login;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto {

    private int id;
    private String userName;
    private String password;
    private String roles;
}
