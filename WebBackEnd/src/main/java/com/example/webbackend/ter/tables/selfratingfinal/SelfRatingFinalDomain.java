package com.example.webbackend.ter.tables.selfratingfinal;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "SELF_RATING_FINAL")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SelfRatingFinalDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "SRSZ_1")
    private int srsz1;

    @Column(name = "SRSZ_2")
    private int srsz2;

    @Column(name = "SRSZ_3")
    private int srsz3;

    @Column(name = "SRSZ_4")
    private int srsz4;

    @Column(name = "SRSZ_5")
    private int srsz5;

    @Column(name = "SRSZ_6")
    private int srsz6;

    @Column(name = "SRSZ_7")
    private int srsz7;

    @Column(name = "SRSZ_8")
    private int srsz8;
}
