import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {LoginResponseDto} from "../../model/login/login-response-dto";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  apiUrl = "http://localhost:8080/login";
  body: object = {};


  set setBody(v: object){
    this.body = v;
  }

  public checkUser(): Observable<LoginResponseDto> {
    return this.httpClient.post<LoginResponseDto>(this.apiUrl + "/user", this.body);
  }

}
