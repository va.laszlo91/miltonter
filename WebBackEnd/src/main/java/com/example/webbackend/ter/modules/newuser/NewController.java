package com.example.webbackend.ter.modules.newuser;

import com.example.webbackend.ter.model.newuser.NewUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/newuser")
public class NewController {

    @Autowired
    NewUserService newUserService;

    @PostMapping("/insert")
    public Boolean updateUser(
            @RequestBody NewUserDto newUserDto
    ) {
        return newUserService.updateUser(newUserDto);
    }
}
