package com.example.webbackend.ter.modules.user;

import com.example.webbackend.ter.model.user.UserDto;
import com.example.webbackend.ter.tables.user.UserDomain;
import com.example.webbackend.ter.tables.user.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    ModelMapper modelMapper = new ModelMapper();

    public UserDto getUser(String userName) {
        return modelMapper.map(userRepository.getUserByName(userName), UserDto.class);
    }

    public Boolean updateUser(UserDto userDto) {
        try {
            UserDomain userDomain = modelMapper.map(userDto, UserDomain.class);
            userRepository.saveAndFlush(userDomain);
            return Boolean.TRUE;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }

}
