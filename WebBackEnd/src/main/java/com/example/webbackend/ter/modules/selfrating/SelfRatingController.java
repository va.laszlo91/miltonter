package com.example.webbackend.ter.modules.selfrating;

import com.example.webbackend.ter.model.common.OneParamDto;
import com.example.webbackend.ter.model.selfrating.SelfRatingTempDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/selfrating")
public class SelfRatingController {

    @Autowired
    SelfRatingService selfRatingService;

    @PostMapping("/inserttotemp")
    public Boolean insertDataToSelfRatingTemp(
            @RequestBody SelfRatingTempDto selRatingTempDto
    ) {
        return selfRatingService.insertDataToSelfRatingTemp(selRatingTempDto);
    }

    @GetMapping("/getalltemp")
    public List<SelfRatingTempDto> getAllTerInfo() {
        return selfRatingService.getAllTempData();
    }

    @PostMapping("/statusz")
    public String userIsValid(
            @RequestBody OneParamDto oneParamDto
    ) {
        return selfRatingService.getSelfRatingStatus(oneParamDto.getParam());
    }
}
