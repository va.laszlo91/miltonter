package com.example.webbackend.ter.tables.terinfo;

import com.example.webbackend.ter.tables.user.UserDomain;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

@Repository
public interface TerInfoRepository extends JpaRepositoryImplementation<TerInfoDomain, Integer> {
}
