package com.example.webbackend.ter.tables.terinfo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TER_INFO")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TerInfoDomain {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "TASK")
    private String task;

    @Column(name = "TYPE")
    private String type;


}
