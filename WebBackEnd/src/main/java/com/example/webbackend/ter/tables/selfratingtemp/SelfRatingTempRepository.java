package com.example.webbackend.ter.tables.selfratingtemp;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SelfRatingTempRepository extends JpaRepositoryImplementation<SelfRatingTempDomain, Integer> {


    @Query("select a.status from SelfRatingTempDomain a "
            + "where a.name = :name")
    String getSelfRatingStatus(
            @Param("name") String name
    );


    @Query("select a.id from SelfRatingTempDomain a "
            + "where a.userName = :userName")
    Integer getIdByUserName(
            @Param("userName") String userName
    );
}
