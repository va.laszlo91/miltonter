package com.example.webbackend.ter.modules.login;


import com.example.webbackend.ter.model.login.LoginDto;
import com.example.webbackend.ter.model.login.LoginResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/user")
    public LoginResponseDto userIsValid(
            @RequestBody LoginDto loginDto
    ){
        return loginService.userIsValid(loginDto);
    }
}
